//Теоретичні питання

//Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
//для позначення спеціального символу починаються зі зворотного слєша \
//або коли в рядок потрібно вставити лапку


//Які засоби оголошення функцій ви знаєте?
// 1. Function declaration -  оголошується за допомогою function
//function sayHello() {
//alert("Hello, world!")}
//sayHello()
// 2. Function expression - оголошується за допомогою function, не має імені, записується в змінну
//const sayHello = function() {
//alert("Hello,world!")}
//sayHello()
// 3. Named Function expression - записується в змінну, має ім'я (не можна буде викликати по ньому)
//const sayHello = function student() {
// alert("Hello, student!")}
//sayHello()

//Що таке hoisting, як він працює для змінних та функцій?
//Hoisting - спливання, підняття, це механізм, при якому змінні та оголошення функції
// піднімаються вгору по своїй області видимості перед виконанням коду.
// Однією з переваг підйому є те, що він дозволяє нам використовувати функції перед їх 
//оголошенням у коді.


//Завдання
//Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
//Завдання має бути виконане на чистому Javascript без використання бібліотек 
//типу jQuery або React.

//Технічні вимоги:
//Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser())
// і доповніть її наступним функціоналом:
//При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) 
//і зберегти її в полі birthday.
//Створити метод getAge() який повертатиме скільки користувачеві років.
//Створити метод getPassword(), який повертатиме першу літеру імені користувача у 
//верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження.
// (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
//Вивести в консоль результат роботи функції createNewUser(), 
//а також функцій getAge() та getPassword() створеного об'єкта.




const createNewUser= () => {
    const newUser = {
    firstName: prompt("What is your firstName?"),
    lastName: prompt("What is your lastName?"),
    dateBirthday: prompt("What is your day of Birthday? Please, enter in the format dd.mm.yyyy"),
    

    getLogin() {
    return `${this.firstName[0]}${this.lastName}`.toLowerCase();  
    },

    getAge() {
    let now = new Date(); //Текущя дата
   let today = new Date(now.getFullYear(), now.getMonth(), now.getDate()); //Текущя дата без времени
    let dob = new Date(this.dateBirthday); //Дата рождения
   let dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate()); //ДР в текущем году
    let age; //Возраст
    //Возраст = текущий год - год рождения
    age = today.getFullYear() - dob.getFullYear();
    //Если ДР в этом году ещё предстоит, то вычитаем из age один год
    if (today < dobnow) 
    {
    age = age-1;
    console.log (this.dateBirthday);
    return age;
    }
    return age;
},

    getPassword() {
   return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.dateBirthday.slice(-4)}`},
    }

    return newUser;
    };

    const usrer1 = createNewUser();
    console.log (usrer1.getLogin());
    console.log (usrer1.getAge());
    console.log (usrer1.getPassword());